#include <ESP8266WiFi.h>
#include "SH1106Wire.h"
#include <EEPROM.h>
//memory allocation
// SSID length (1 byte) | password length (1 byte) | *ssid (n byte) | *password (m byte)
#define SSID_LEN_ADDR 0
#define PASSWORD_LEN_ADDR 1

//set the RESET to 0 if you dont want to reset, set it 1 if you want to reset
#define RESET 1
#define BTN D5
#define SPK D7
#define LED D6

//wifi settings. SET IT IN THE CONTROLLER APP!!!
char *ssid = "ESP8266_WiFi", *password = "12345678";

SH1106Wire display(0x3c, SDA, SCL);

#define PORT 9654  //Port number
WiFiServer server(PORT);

void setup() {
  EEPROM.begin(512);  //Initialize EEPROM
  Serial.begin(115200);

  tryFactoryReset();
  //setWiFi("Charter Informatika 2019", "charter1");
  set_SSID_PASSWORD();
  startWiFi();

  initOLED();

  pinMode(BTN, INPUT_PULLUP);
  pinMode(LED, OUTPUT);

  Serial.println("Initializing server...");
  server.begin();
  Serial.println("Done.");

  printString(String("Done.\n") + String("IP: ") + WiFi.localIP().toString());
}

void loop() {
  handleClient();
}

void handleClient() {
  WiFiClient client = server.available();

  if (client && client.connected()) {
    Serial.println("Client connected.");
    delay(500);
  }
  else return;

  while (client.connected()) {
    String packet = "";
    if (client.available() > 0)
      Serial.println("Reading network traffic...");
    while (client.available() > 0) {
      packet += (char)client.read();
      delay(3);
    }

    if (packet != "") {
      Serial.print("Packet: ");
      Serial.println(packet);

      if (!packet.startsWith("LED"))
        printString(packet);
    }

    if (packet.startsWith("DSC")) {
      client.stop();
      Serial.println("Client disconnected.");
      return;
    }
    else if (packet.startsWith("LED")) {
      int i = packet.lastIndexOf(' ') + 1;
      int ledVal = packet.substring(i).toInt();
      Serial.print("LED set to "); Serial.println(ledVal);
      analogWrite(LED, ledVal);
    }

    sendButtonState(&client);
  }
}

void sendButtonState(WiFiClient *client) {
  if (getBtn()) {
    client->println("ON");
  }
  else {
    client->println("OFF");
  }
  delay(20);
}

bool getBtn() {
  return !digitalRead(BTN);
}

void printString(String msg) {
  display.clear();
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.drawString(0, 0, msg);
  display.display();
}

void initOLED() {
  Serial.println("Initializing OLED display...");
  display.init();
  display.flipScreenVertically();
  display.setFont(ArialMT_Plain_10);
  Serial.println("Done.");
}

void setWiFi(char *_ssid, char *_password) {
  byte ssidLen = strlen(_ssid);
  int ssidAddr = 2;
  byte passwordLen = strlen(_password);
  int passwordAddr = 2 + ssidLen + 1;
  if (passwordLen < 8) {
    Serial.println("Password must be more than 7 characters long.");
    delay(1000);
    return;
  }

  EEPROM.write(SSID_LEN_ADDR, ssidLen);
  EEPROM.write(PASSWORD_LEN_ADDR, passwordLen);

  for (int i = 0; i < ssidLen; i++) {
    EEPROM.write(ssidAddr + i, _ssid[i]);
  }
  for (int i = 0; i < passwordLen; i++) {
    EEPROM.write(passwordAddr + i, _password[i]);
  }

  Serial.println("SSID and password set.");
  Serial.print("New SSID: ");
  Serial.println(_ssid);
  Serial.print("New password: ");
  Serial.println(_password);
  EEPROM.commit();
  delay(100);

  resetESP8266();
}

void resetESP8266() {
  Serial.println("Resetting ESP..."); delay(1000);
  ESP.restart();
}

void startWiFi() {
  byte ssidLen = EEPROM.read(SSID_LEN_ADDR);

  if (ssidLen) {
    WiFi.begin(ssid, password);             // Connect to the network
    Serial.print("Connecting to ");
    Serial.print(ssid);

    int retries = 0;
    do {
      if (Serial.available() && Serial.read() == 's')
        setWiFi("Charter Informatika 2019", "charter1");

      delay(1000);
      Serial.print('.');
      if (retries++ > 10) {
        Serial.println("Couldnt connect to WiFi network. Resetting...");
        delay(3000);
        resetESP8266();
      }
    } while (WiFi.status() != WL_CONNECTED);

    Serial.println("\nConnection established!");
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
  }
  else {
    WiFi.mode(WIFI_AP);
    WiFi.softAP(ssid, password, 1, 0);
    Serial.println("WiFi started.");

    /*delay(1000);
      setWiFi("Charter Informatika 2019", "charter1")*/;
  }
}

void set_SSID_PASSWORD() {
  byte ssidLen = EEPROM.read(SSID_LEN_ADDR);
  int ssidAddr = 2;
  byte passwordLen = EEPROM.read(PASSWORD_LEN_ADDR);
  int passwordAddr = 2 + ssidLen + 1;

  if (ssidLen) {
    ssid = (char *)malloc(ssidLen * sizeof(char));
    for (int i = 0; i < ssidLen; i++) {
      ssid[i] = EEPROM.read(i + ssidAddr);
    }
    password = (char *)malloc(passwordLen * sizeof(char));
    for (int i = 0; i < passwordLen; i++) {
      password[i] = EEPROM.read(i + passwordAddr);
    }
  }
  else {
    Serial.println("Missing WiFi settings. Using default WiFi settings.");
  }
  Serial.print("SSID: ");
  Serial.println(ssid);
  Serial.print("Password: ");
  Serial.println(password);
}

void tryFactoryReset() {
  if (RESET) {
    for (int i = 0; i < 255; i++) {
      EEPROM.write(i, 0);
    }
    EEPROM.commit();
    Serial.print("Factory reset complete.");
    Serial.println(EEPROM.read(0));
  }
}
