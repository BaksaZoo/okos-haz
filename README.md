# Olvass el
Esp folyamatosan küldi a gomb állapotát: 

    while (client.connected()) {
        sendButtonState(&client);
    }
Telefonos app kiolvassa az érkezett csomagot. Ha a csomag "ON" vagy "OFF" akkor módosítja a gomb állapotát jelző vezérlőt.
A többi kód egyéb funkciókat valósít meg (nem lényeges).
