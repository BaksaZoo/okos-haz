package com.example.continous_esp8266_communication;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.ToggleButton;

public class ConnectTask extends AsyncTask<String, String, TcpClient> {

    public TcpClient mTcpClient;
    private ToggleButton chkLedState;
    private String ipAddress;

    public ConnectTask(ToggleButton chkLedState, String ipAddress) {
        this.chkLedState = chkLedState;
        this.ipAddress = ipAddress;
    }

    @Override
    protected TcpClient doInBackground(String... strings) {
        //we create a TCPClient object
        mTcpClient = new TcpClient(new TcpClient.OnMessageReceived() {
            @Override
            //here the messageReceived method is implemented
            public void messageReceived(String message) {
                //this method calls the onProgressUpdate
                publishProgress(message);
            }
        }, ipAddress);
        mTcpClient.run();

        return null;
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
        //response received from server
        Log.d("test", "response " + values[0]);
        chkLedState.post(new Runnable() {
            @Override
            public void run() {
                if ("ON".equals(values[0]))
                    chkLedState.setChecked(true);
                else
                    chkLedState.setChecked(false);
            }
        });
        //process server response here....

    }
}
