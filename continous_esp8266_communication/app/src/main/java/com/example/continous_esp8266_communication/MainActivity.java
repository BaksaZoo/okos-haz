package com.example.continous_esp8266_communication;

import androidx.appcompat.app.AppCompatActivity;

import android.net.sip.SipSession;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.SeekBar;

public class MainActivity extends AppCompatActivity {

    ConnectTask connectTask;
    EditText txtStrPacket;
    EditText txtIpAddress;
    SeekBar skbrLedBrightness;
    SeekBar.OnSeekBarChangeListener skbrLedBrightness_change = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (connectTask.mTcpClient != null) {
                connectTask.mTcpClient.sendMessage("LED " + progress);
                try {
                    Thread.sleep(30);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else
                Log.d("SKBR_CHANGE", "mTcpClient is null");
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtStrPacket = findViewById(R.id.txtStrPacket);;
        skbrLedBrightness = findViewById(R.id.skbrLedBrightness);
        skbrLedBrightness.setOnSeekBarChangeListener(skbrLedBrightness_change);
        txtIpAddress = findViewById(R.id.txtIpAddress);
    }

    public void btnConnect_click(View view){
        String ipAddress = txtIpAddress.getText().toString();
        connectTask = new ConnectTask(findViewById(R.id.chkLedState), ipAddress);
        connectTask.execute("");
    }

    public void btnSend_click(View view) {
        if (connectTask.mTcpClient != null) {
            connectTask.mTcpClient.sendMessage(txtStrPacket.getText().toString());
            txtStrPacket.setText("");
        } else
            Log.d("BTN_CLICK", "mTcpClient is null");
    }
}